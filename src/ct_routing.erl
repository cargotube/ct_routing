-module(ct_routing).

-include_lib("ct_msg/include/ct_msg.hrl").

-export([handle_hello/2, handle_authenticate/2, handle_established/3,
         handle_session_closed/1, agent_identification/0, agent_roles/0, to_session/2, to_peer/2]).

agent_identification() ->
    Version = get_agent_version(),
    Agent = get_agent_name(),
    Dash = <<"-">>,
    <<Agent/binary, Dash/binary, Version/binary>>.

agent_roles() ->
    #{broker =>
          #{features =>
                #{subscriber_blackwhite_listing => true,
                  publisher_exclusion => true,
                  publisher_identification => true,
                  subscription_meta_api => true,
                  session_meta_api => true,
                  pattern_based_subscription => false,
                  event_history => false,
                  publication_trustlevels => false,
                  sharded_subscription => false}},
      dealer =>
          #{features =>
                #{caller_identification => true,
                  registration_meta_api => true,
                  session_meta_api => true,
                  progressive_call_results => false,
                  call_canceling => false,
                  call_timeout => false,
                  testament_meta_api => false,
                  call_trustlevel => false,
                  pattern_based_registration => false,
                  shared_registration => false,
                  sharded_registration => false}}}.

handle_hello(Hello, Session) ->
    AuthResult = ct_auth:handle_hello(Hello, Session),
    handle_auth_result(AuthResult, Session).

handle_authenticate(Authenticate, Session) ->
    {_Time, Result} = timer:tc(fun do_handle_authenticate/2, [Authenticate, Session]),
    Result.

handle_established(Type, Message, Session) ->
    {_Time, Result} = timer:tc(fun do_handle_established/3, [Type, Message, Session]),
    Result.

do_handle_authenticate(Authenticate, Session) ->
    AuthResult = ct_auth:handle_authenticate(Authenticate, Session),
    handle_auth_result(AuthResult, Session).

do_handle_established(Type, Message, Session) ->
    ctr_routing:handle_established(Type, Message, Session).

handle_session_closed(Session) ->
    close_session(Session),
    ok.

close_session(Session) ->
    %ctr_broker:unsubscribe_all(Session),
    %ctr_dealer:unregister_all(Session),
    %ctr_broker:send_session_meta_event(leave, Session),
    cta_session:close(Session).

handle_auth_result({ok, Session}, _PeerAtGate) ->
    SessionId = cta_session:get_id(Session),
    Details =
        #{agent => ct_routing:agent_identification(), roles => ct_routing:agent_roles()},
    ok = ctr_broker:send_session_meta_event(join, Session),
    to_session(Session, ?WELCOME(SessionId, Details));
handle_auth_result({challenge, AuthMethod, Extra, Session}, _PeerAtGate) ->
    to_session(Session, ?CHALLENGE(AuthMethod, Extra));
handle_auth_result({abort, Reason}, PeerAtGate) ->
    message_to_peer(PeerAtGate, ?ABORT(#{}, Reason)).

to_session(Session, Message) ->
    PeerAtGate = cta_session:get_peer(Session),
    to_peer([PeerAtGate], {to_peer, Message}).

message_to_peer(Peer, Message) ->
    to_peer([Peer], {to_peer, Message}).

to_peer([], _Message) ->
    ok;
to_peer([PeerAtGate | Tail], Message) ->
    PeerAtGate ! Message,
    to_peer(Tail, Message);
to_peer(PeerAtGate, Message) ->
    to_peer([PeerAtGate], Message).

get_agent_version() ->
    Version = safe_version(application:get_key(cargotube, vsn)),
    ensure_binary(Version).

safe_version(undefined) ->
    <<"development">>;
safe_version({ok, Version}) ->
    Version.

get_agent_name() ->
    <<"CargoTube.org">>.

ensure_binary(Binary) when is_binary(Binary) ->
    Binary;
ensure_binary(List) when is_list(List) ->
    list_to_binary(List).
