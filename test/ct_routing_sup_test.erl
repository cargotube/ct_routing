-module(ct_routing_sup_test).

-include_lib("eunit/include/eunit.hrl").

init_test() ->
    Result = ct_routing_sup:init(noparams),
    {ok, {Map, [InvocationSup, DataCleaner]}} = Result,
    ?assertEqual(maps:size(Map), 0),
    ?assertEqual(maps:get(id, InvocationSup), invocation_sup),
    ?assertEqual(maps:get(type, InvocationSup, worker), supervisor),
    ?assertEqual(maps:get(id, DataCleaner), cleaner),
    ?assertEqual(maps:get(type, DataCleaner, worker), worker).
