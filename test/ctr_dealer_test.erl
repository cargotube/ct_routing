-module(ctr_dealer_test).

-include_lib("eunit/include/eunit.hrl").
-include_lib("ct_msg/include/ct_msg.hrl").


invocation_error_test() ->
    {ok, Mock} = start_mock(),
    try
        Error = ?ERROR(invocation, 4664054645854882, #{} , <<"wamp.error.runtime_error">>, 
		       [<<"git Exception Delete failed">>], #{ <<"key">> => <<"value">> }),
        ok = ctr_dealer:handle_message(error, Error, session),
	ok
    after
        stop_mock(Mock)
    end,
    ok.

start_mock() ->
    Modules = [ctrd_invocation, cta_session],
    ct_test_utils:meck_new(Modules),

    GetInvocation = fun(_session) -> {error, not_found} end,
    GetRealm = fun(_session) -> <<"devrealm">> end,

    ok = meck:expect(ctrd_invocation, get_invocation_callees, GetInvocation),
    ok = meck:expect(cta_session, get_realm, GetRealm),
    {ok, Modules}.



stop_mock(Modules) ->
    ct_test_utils:meck_done(Modules),
    ok.
